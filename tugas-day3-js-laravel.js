//soal 1
var daftarBuah = [
  "2. Apel",
  "5. Jeruk",
  "3. Anggur",
  "4. Semangka",
  "1. Mangga",
];
daftarBuah.sort();
daftarBuah.forEach((element) => {
  console.log(element);
});

//soal 2
var kalimat = "saya sangat senang belajar javascript";
console.log(kalimat.split(" "));

//soal 3
let jawaban = [
  { nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000 },
  { nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000 },
  { nama: "semangka", warna: "hijau & merah", "ada bijinya": "ada", harga: 10000,},
  { nama: "pisang", warna: "kuning", "ada bijinya": "tidak", harga: 5000 },
];

console.log(jawaban[0]);
