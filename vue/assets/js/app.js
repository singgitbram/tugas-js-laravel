var app = new Vue({
  el: "#app",
  data: {
    comments: [
      {
        content: "ini komen 1",
        score: 1,
        date: "10-11-2020",
        upVoted: false,
        downVoted: false,
      },
      {
        content: "ini komen 2",
        score: 6,
        date: "11-11-2020",
        upVoted: false,
        downVoted: false,
      },
      {
        content: "ini komen 3",
        score: 5,
        date: "13-11-2020",
        upVoted: false,
        downVoted: false,
      },
    ],
    newComment: "",
  },

  methods: {
    addCommentFunction: function () {
      let obj = {
        content: this.newComment,
        score: 0,
        date: "14-11-2020",
        upVoted: false,
        downVoted: false,
      };
      this.comments.push(obj);
      this.newComment = "";
    },
    upVoteFunction: function (index) {
      if (
        this.comments[index].upVoted == false &&
        this.comments[index].downVoted == false
      ) {
        this.comments[index].score++;
        this.comments[index].upVoted = true;
      } else if (
        this.comments[index].upVoted == false &&
        this.comments[index].downVoted == true
      ) {
        this.comments[index].score += 2;
        this.comments[index].upVoted = true;
        this.comments[index].downVoted = false;
      } else {
        this.comments[index].score--;
        this.comments[index].upVoted = false;
      }
    },
    downVoteFunction: function (index) {
      if (
        this.comments[index].upVoted == false &&
        this.comments[index].downVoted == false
      ) {
        this.comments[index].score--;
        this.comments[index].downVoted = true;
      } else if (
        this.comments[index].downVoted == false &&
        this.comments[index].upVoted == true
      ) {
        this.comments[index].score -= 2;
        this.comments[index].downVoted = true;
        this.comments[index].upVoted = false;
      } else {
        this.comments[index].score++;
        this.comments[index].downVoted = false;
      }
    },
  },
});
